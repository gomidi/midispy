module gitlab.com/gomidi/midispy/cmd/midispy

go 1.14

replace gitlab.com/gomidi/midispy => ../../

require (
	gitlab.com/gomidi/midi v1.23.7
	gitlab.com/gomidi/midicatdrv v0.3.7 // indirect
	gitlab.com/gomidi/midispy v1.11.1
	gitlab.com/metakeule/config v1.21.0
)
