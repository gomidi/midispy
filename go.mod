module gitlab.com/gomidi/midispy

go 1.14

require (
	gitlab.com/gomidi/midi v1.23.7
	gitlab.com/gomidi/midicatdrv v0.3.7 // indirect
	gitlab.com/gomidi/rtmididrv v0.13.0
	gitlab.com/gomidi/rtmididrv/imported/rtmidi v0.0.0-20191025100939-514fe0ed97a6 // indirect
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
)
