package midispy

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/reader"
)

// Run will read the messages from the given mid.In port,
// pass them to the given mid.Reader and given a mid.Out port
// that is not nil, write them to the out port.
// All given port must be opened. Run will not close any ports.
// Stop the spying by closing the ports.
func Run(in midi.In, out midi.Out, rd *reader.Reader) error {
	if in == nil {
		panic("MIDI in port is nil")
	}

	//reader.NoLogger()(rd)

	s := &spy{
		out: out,
		In:  in,
	}

	return rd.ListenTo(s)
}

// spy connects a MIDI in port and a MIDI out port and allows
// inspecting the MIDI messages flowing from in to out with the help of
// a mid.Reader.
type spy struct {
	out midi.Out
	midi.In
}

// SetListener sets the given outer function as listener
func (s *spy) SetListener(outer func([]byte, int64)) (err error) {

	var listener func([]byte, int64)

	switch {
	case s.out == nil && outer == nil:
		listener = func(bt []byte, deltamicrosecs int64) {}
	case s.out == nil && outer != nil:
		listener = func(bt []byte, deltamicrosecs int64) { outer(bt, deltamicrosecs) }

	case s.out != nil && outer == nil:
		listener = func(bt []byte, deltamicrosecs int64) {
			s.out.Write(bt)
		}
	case s.out != nil && outer != nil:
		listener = func(bt []byte, deltamicrosecs int64) {
			outer(bt, deltamicrosecs)
			s.out.Write(bt)
		}
	}

	if listener != nil {
		fmt.Println("setting listener")
		err = s.In.SetListener(listener)
		if err != nil {
			return
		}
	}
	return nil
}
